# Usecase diagrams

## 0. User Hierarchy

```plantuml
@startuml user_hierarchy
actor "User" as u
actor "Registered User" as ru
actor "New User" as nu
actor "Student" as student
actor "Student(not Child)" as parent
actor "Student(Child)" as child
actor "Teacher" as teacher
actor "Staff" as staff
actor "Admin" as admin
actor "Super Admin" as super_admin

u <|-- ru
u <|-- nu
ru <|-- student
ru <|-- teacher
ru <|-- staff
student <|-- parent
student <|-- child
staff <|-- admin
admin <|-- super_admin
@enduml
```

## 1. User service

```plantuml
@startuml user_service
left to right direction
actor "Registered User" as ru
actor "New User" as nu
actor "Super Admin" as super_admin

package "User Service" <<Subsystem>> {
    usecase "Register Student" as register
    usecase "Update User" as update
    usecase "View Users" as view
    usecase "Search Users" as search
    usecase "Delete User" as delete
    usecase "Lock User" as lock
    usecase "Restore User" as restore
    usecase "View User Detail" as detail
    usecase "View Self Detail" as self_detail
    usecase "Authenticate" as auth
}

super_admin -|> ru

self_detail -- ru
register -- nu
update -- ru
view -- super_admin
search ..> view: <<extend>>
detail ..> view: <<extend>>
delete ..> view: <<extend>>
lock ..> view: <<extend>>
restore ..> delete: <<extend>>

auth <.. self_detail: <<include>>
auth <.. update: <<include>>
auth <.. detail: <<include>>
auth <.. view: <<include>>
auth <.. search: <<include>>
auth <.. delete: <<include>>
auth <.. lock: <<include>>
auth <.r. restore: <<include>>

@enduml
```

## 2. Teacher service

```plantuml
@startuml teacher_service
left to right direction
actor "Staff" as staff

package "Teacher Service" <<Subsystem>> {
    usecase "Create Teachers" as create
    usecase "View Teachers" as view
    usecase "Search Teachers" as search
    usecase "Delete Teacher" as delete
    usecase "Lock Teacher" as lock
    usecase "Restore Teacher" as restore
    usecase "View Detail" as detail
    usecase "Authenticate" as auth
}

create -- staff
view -- staff
search ..> view: <<extend>>
detail ..> view: <<extend>>
delete ..> view: <<extend>>
lock ..> view: <<extend>>
restore ..> delete: <<extend>>

auth <.. create: <<include>>
auth <.. detail: <<include>>
auth <.. view: <<include>>
auth <.. search: <<include>>
auth <.. delete: <<include>>
auth <.. lock: <<include>>
auth <. restore: <<include>>
@enduml
```

## 3. Staff service

```plantuml
@startuml staff_service
left to right direction
actor "Admin" as admin

package "Staff Service" <<Subsystem>> {
    usecase "Create Staffs" as create
    usecase "View Staffs" as view
    usecase "Search Staffs" as search
    usecase "Delete Staff" as delete
    usecase "Lock Staff" as lock
    usecase "Restore Staff" as restore
    usecase "View Detail" as detail
    usecase "Authenticate" as auth
}

create -- admin
view -- admin
search ..> view: <<extend>>
detail ..> view: <<extend>>
delete ..> view: <<extend>>
lock ..> view: <<extend>>
restore ..> delete: <<extend>>

auth <.. create: <<include>>
auth <.. detail: <<include>>
auth <.. view: <<include>>
auth <.. search: <<include>>
auth <.. delete: <<include>>
auth <.. lock: <<include>>
auth <.r. restore: <<include>>
@enduml
```

## 4. Children service

```plantuml
@startuml children_service
left to right direction
actor "Student(not Child)" as student

package "Children Service" <<Subsystem>> {
    usecase "Create Child" as create
    usecase "View Children" as view
    usecase "Search Children" as search
    usecase "Delete Child" as delete
    usecase "Lock Child" as lock
    usecase "Restore Child" as restore
    usecase "View Detail" as detail
    usecase "Authenticate" as auth
}

create -- student
view -- student
search ..> view: <<extend>>
detail ..> view: <<extend>>
delete ..> view: <<extend>>
lock ..> view: <<extend>>
restore ..> delete: <<extend>>

auth <.. create: <<include>>
auth <.. detail: <<include>>
auth <.. view: <<include>>
auth <.. search: <<include>>
auth <.. delete: <<include>>
auth <.. lock: <<include>>
auth <.r. restore: <<include>>
@enduml
```

## 5. Art service

```plantuml
@startuml art_service
left to right direction
actor "User" as u
actor "Super Admin" as sa

package "Art Service" <<Subsystem>> {
    usecase "Create Art Level" as create_level
    usecase "View Art Levels" as view_level
    usecase "Search Art Levels" as search_level
    usecase "Update Art Level" as update_level
    usecase "Delete Art Level" as delete_level
    usecase "Create Art Type" as create_type
    usecase "View Art Types" as view_type
    usecase "Search Art Types" as search_type
    usecase "Update Art Type" as update_type
    usecase "Delete Art Type" as delete_type
    usecase "Authenticate" as auth
}

sa --|> u

view_level -- u
view_type -- u
create_level -- sa
update_level -- sa
delete_level -- sa
create_type -- sa
update_type -- sa
delete_type -- sa
search_level ..> view_level: <<extend>>
search_type ..> view_type: <<extend>>
update_level ..> view_level: <<include>>
delete_level ..> view_level: <<include>>
update_type ..> view_type: <<include>>
delete_type ..> view_type: <<include>>

auth <.. create_level: <<include>>
auth <.. update_level: <<include>>
auth <.. delete_level: <<include>>
auth <.. create_type: <<include>>
auth <.. update_type: <<include>>
auth <.. delete_type: <<include>>
@enduml
```

## 6. Contest service

```plantuml
@startuml contest_service
left to right direction
actor "User" as u
actor "Staff" as staff

package "Contest Service" <<Subsystem>> {
    usecase "Create Contests" as create
    usecase "View Contests" as view
    usecase "Search Contests" as search
    usecase "Delete Contest" as delete
    usecase "Update Contest" as update
    usecase "View Detail Contest" as detail
    usecase "Authenticate" as auth
}

staff --|> u

view -- u
create -- staff
delete -- staff
update -- staff
search ..> view: <<extend>>
detail ..> view: <<extend>>
delete ..> view: <<include>>
update ..> view: <<include>>

auth <.. create: <<include>>
auth <.. update: <<include>>
auth <.. view: <<include>>
auth <.. search: <<include>>
auth <.. delete: <<include>>
@enduml
```

## 7. Course service

```plantuml
@startuml course_service
left to right direction
actor "User" as u
actor "Staff" as staff
actor "Teacher" as teacher
actor "Admin" as admin

package "Course Service" <<Subsystem>> {
    usecase "Create Courses" as create
    usecase "View Courses" as view
    usecase "Update Courses" as update
    usecase "Search Courses" as search
    usecase "Delete Course" as delete
    usecase "Register Teach Course" as register_teach
    usecase "Review Register Teach Course" as review_regiter_teach
    usecase "View Detail Course" as detail
    usecase "Authenticate" as auth
}

teacher --|> u
staff --|> u
admin --|> staff

view -- u
create -- admin
delete -- admin
update -- admin
review_regiter_teach -- staff
register_teach -- teacher
register_teach .r.> view: <<include>>
delete ..> view: <<include>>
update ..> view: <<include>>
search ..> view: <<extend>>
detail ..> view: <<extend>>
review_regiter_teach ..> register_teach: <<include>>

auth <.. create: <<include>>
auth <.. update: <<include>>
auth <.. delete: <<include>>
auth <.. register_teach: <<include>>
auth <.. review_regiter_teach: <<include>>
@enduml
```

## 8. Schedule service

```plantuml
@startuml schedule_service
left to right direction
actor "User" as u
actor "Staff" as staff
actor "Super Admin" as super_admin

package "Schedule Service" <<Subsystem>> {
    usecase "Create Schedule" as create_schedule
    usecase "View Schedules" as view_schedule
    usecase "Search Schedules" as search_schedule
    usecase "Update Schedule" as update_schedule
    usecase "Delete Schedule" as delete_schedule
    usecase "View Detail Schedule" as detail_schedule
    usecase "Create Lesson Time" as create_time
    usecase "View Lesson Times" as view_time
    usecase "Search Lesson Times" as search_time
    usecase "Update Lesson Time" as update_time
    usecase "Delete Lesson Time" as delete_time
    usecase "View Detail Lesson Time" as detail_time
    usecase "Authenticate" as auth
}

staff --|> u
super_admin --|> staff

view_time -- u
view_schedule -- u
create_time -- super_admin
update_time -- super_admin
delete_time -- super_admin
create_schedule -- staff
update_schedule -- staff
delete_schedule -- staff
search_time ..> view_time: <<extend>>
detail_time ..> view_time: <<extend>>
delete_time ..> view_time: <<include>>
update_time ..> view_time: <<include>>
search_schedule ..> view_schedule: <<extend>>
detail_schedule ..> view_schedule: <<extend>>
delete_schedule ..> view_schedule: <<include>>
update_schedule ..> view_schedule: <<include>>

auth <.. create_time: <<include>>
auth <.. update_time: <<include>>
auth <.. delete_time: <<include>>
auth <.. create_schedule: <<include>>
auth <.. update_schedule: <<include>>
auth <.. delete_schedule: <<include>>
@enduml
```

## 9. Semester service

```plantuml
@startuml semester_service
left to right direction
actor "User" as u
actor "Staff" as staff
actor "Admin" as admin
actor "Teacher" as teacher
actor "Student" as student

package "Semester Service" <<Subsystem>> {
    usecase "Create Semester Course" as create_course
    usecase "View Semester Courses" as view_course
    usecase "Search Semester Courses" as search_course
    usecase "Update Semester Course" as update_course
    usecase "Delete Semester Course" as delete_course
    usecase "View Detail Semester Course" as detail_course
    usecase "Create Semester" as create_semester
    usecase "View Semesters" as view_semester
    usecase "Search Semesters" as search_semester
    usecase "Update Semester" as update_semester
    usecase "Delete Semester" as delete_semester
    usecase "View Detail Semester" as detail_semester
    usecase "Review Semester Creation" as review_semester
    usecase "Register Teach Semester" as register_teach_semester
    usecase "Register Join Semester" as register_join_semester
    usecase "Authenticate" as auth
}

staff --|> u
teacher --|> u
student --|> u
admin --|> staff

view_semester -- u
view_course -- u
create_semester -- staff
update_semester -- staff
delete_semester -- staff
create_course -- staff
update_course -- staff
delete_course -- staff
register_teach_semester -- teacher
register_join_semester -- student
review_semester -- admin
register_teach_semester ..> create_course: <<include>>
register_join_semester ..> create_course: <<include>>
review_semester ..> create_semester: <<include>>
search_semester ..> view_semester: <<extend>>
detail_semester ..> view_semester: <<extend>>
delete_semester ..> view_semester: <<include>>
update_semester ..> view_semester: <<include>>
search_course ..> view_course: <<extend>>
detail_course ..> view_course: <<extend>>
delete_course ..> view_course: <<include>>
update_course ..> view_course: <<include>>

auth <. register_teach_semester: <<include>>
auth <. register_join_semester: <<include>>
auth <.. review_semester: <<include>>
auth <.. create_semester: <<include>>
auth <.. update_semester: <<include>>
auth <.. delete_semester: <<include>>
auth <.. create_course: <<include>>
auth <.. update_course: <<include>>
auth <.. delete_course: <<include>>
@enduml
```

## 10. Class service

```plantuml
@startuml class_service
left to right direction
actor "Registered User" as ru
actor "Staff" as staff

package "Class Service" <<Subsystem>> {
    usecase "Create Class" as create_class
    usecase "View Classes" as view_class
    usecase "Search Classes" as search_class
    usecase "Update Class" as update_class
    usecase "Delete Class" as delete_class
    usecase "View Detail Class" as detail_class
    usecase "Review Semester Creation" <<@ref semester_service>> as review_semester
    usecase "Register Teach Semester" <<@ref semester_service>> as register_teach_semester
    usecase "Register Join Semester" <<@ref semester_service>> as register_join_semester
    usecase "Authenticate" as auth
}

staff --|> ru

view_class -- ru
create_class -- staff
update_class -- staff
delete_class -- staff
create_class ..> register_teach_semester: <<include>>
create_class ..> register_join_semester: <<include>>
create_class ..> review_semester: <<include>>
search_class ..> view_class: <<extend>>
detail_class ..> view_class: <<extend>>
update_class ..> view_class: <<include>>
delete_class ..> view_class: <<include>>

auth <.. create_class: <<include>>
auth <.. update_class: <<include>>
auth <.. delete_class: <<include>>
@enduml
```

## 11. Section service

```plantuml
@startuml section_service
left to right direction
actor "Registered User" as ru
actor "Staff" as staff
actor "Teacher" as teacher

package "Section Service" <<Subsystem>> {
    usecase "Create Section" as create_section
    usecase "Create Make up Section" as create_makeup_section
    usecase "View Sections" as view_section
    usecase "Search Sections" as search_section
    usecase "Update Section" as update_section
    usecase "Delete Section" as delete_section
    usecase "View Detail Section" as detail_section
    usecase "Create Section Template" as create_template
    usecase "View Section Templates" as view_template
    usecase "Search Section Templates" as search_template
    usecase "Update Section Template" as update_template
    usecase "Delete Section Template" as delete_template
    usecase "View Detail Section Template" as detail_template
    usecase "View Classes" <<@ref class_service>> as view_class
    usecase "Authenticate" as auth
}

staff --|> ru
teacher --|> ru

view_section -- ru
create_section -- staff
create_makeup_section -- teacher
update_section -- teacher
delete_section -- teacher
view_template -- teacher
create_template -- teacher
search_template ..> view_template: <<extend>>
detail_template ..> view_template: <<extend>>
update_template ..> view_template: <<extend>>
delete_template ..> view_template: <<extend>>
view_section .> view_class: <<include>>
create_makeup_section ..> view_section: <<extend>>
search_section ..> view_section: <<extend>>
detail_section ..> view_section: <<extend>>
update_section ..> view_section: <<include>>
delete_section ..> view_section: <<include>>

auth <.. view_template: <<include>>
auth <.. create_template: <<include>>
auth <.. create_makeup_section: <<include>>
auth <.. search_template: <<include>>
auth <.. detail_template: <<include>>
auth <.. update_template: <<include>>
auth <.. delete_template: <<include>>
auth <.. create_section: <<include>>
auth <.. update_section: <<include>>
auth <.. delete_section: <<include>>
@enduml
```
