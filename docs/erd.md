# ERD diagrams

## 1. User service (Teacher, Staff, Children Service)

```plantuml
@startuml user_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    * id: uuid <<PK>>
    --
    parent_id: uuid <<FK>>
    creator_id: uuid <<FK>>
    * email: varchar(255) <<UK>>
    * username: varchar(255) <<UK>>
    * password: varchar(255)
    first_name: varchar(255)
    last_name: varchar(255)
    sex: char(1)
    phone: varchar(15)
    address: varchar(255)
    date_of_birth: date
    profile_image_url: varchar(255)
    * create_time: timestamp
    * update_time: timestamp
    * is_online: boolean
    * is_deleted: boolean
    * is_account_non_expired: boolean
    * is_account_non_locked: boolean
    * is_credit_card_non_expired: boolean
    * is_enabled: boolean
}

entity "ROLE" as role {
    * id: uuid <<PK>>
    --
    * name: varchar(255)
    * description: text
}

entity "USER_HAS_ROLE" as user_has_role {
    * user_id: uuid <<PK,FK>>
    * role_id: uuid <<PK,FK>>
}

' Authenticatin Relationship

user ||..o{ user: has\n children
user ||..o{ user: create user
user_has_role }o--|| user
user_has_role }o--|| role
@enduml
```

## 2. Art service

```plantuml
@startuml art_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "ART_LEVEL" as art_level {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
}

entity "ART_TYPE" as art_type {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
}

' Art Relationship

user ||..o{ art_level: create level
user ||..o{ art_type: create type
@enduml
```

## 3. Contest service

```plantuml
@startuml contest_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "ART_LEVEL" as art_level {
    @ref art_service
}

entity "ART_TYPE" as art_type {
    @ref art_service
}

entity "CONTEST" as contest {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * art_level_id: uuid <<FK>>
    * art_type_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * max_participant: integer
    * registration_time: timestamp
    image_url: varchar(255)
    * start_time: timestamp
    * end_time: timestamp
    * create_time: timestamp
    * update_time: timestamp
    * is_enabled: boolean
}

' Contest Relationship

user ||..o{ contest: create contest
contest }o..|| art_level
contest }o..|| art_type
@enduml
```

## 4. Course service

```plantuml
@startuml course_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "ART_LEVEL" as art_level {
    @ref art_service
}

entity "ART_TYPE" as art_type {
    @ref art_service
}

entity "COURSE" as course {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * art_level_id: uuid <<FK>>
    * art_type_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * max_participant: integer
    * num_of_section: integer
    * price: float8
    image_url: varchar(255)
    * create_time: timestamp
    * update_time: timestamp
    * is_enabled: boolean
}

entity "USER_REGISTER_TEACH_COURSE" as user_register_teach_course {
    * id: uuid <<PK>>
    --
    * teacher_id: uuid <<FK>>
    * course_id: uuid <<FK>>
    reviewer_id: uuid <<FK>>
    * status: char(4)
    * create_time: timestamp
    * update_time: timestamp
}

' Course Relationship

user ||..o{ course: create course
user_register_teach_course }o..|| user: teach
user_register_teach_course }o..|| user: review
user_register_teach_course }o..|| course
course }o..|| art_level
course }o..|| art_type
@enduml
```

## 5. Schedule service

```plantuml
@startuml schedule_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "SCHEDULE" as schedule {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

entity "SCHEDULE_ITEM" as schedule_item {
    * id: uuid <<PK>>
    --
    * schedule_id: uuid <<FK>>
    * lesson_time: uuid <<FK>>
    * date_of_week: integer
}

entity "LESSON_TIME" as lesson_time {
    * id: uuid <<PK>>
    --
    * start_time: time
    * end_time: time
}
' Semester Relationship

user ||..o{ schedule: create schedule
schedule ||..o{ schedule_item
schedule_item ||..o{ lesson_time
@enduml
```

## 6. Semester service

```plantuml
@startuml semester_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "COURSE" as course {
    @ref course_service
}

entity "SEMESTER_CREATION" as semester_creation {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * number: integer <<UK>>
    * year: integer
    * name: varchar(255)
    * description: text
    * start_time: timestamp
    * create_time: timestamp
    * update_time: timestamp
}

entity "USER_REVIEW_CREATE_SEMESTER" as user_review_create_semester {
    * reviewer_id: uuid <<PK,FK>>
    * creation_id: uuid <<PK,FK>>
    --
    * time: timestamp
}

entity "SEMESTER_COURSE" as semester_course {
    * id: uuid <<PK>>
    --
    * creation_id: uuid <<FK>>
    * course_id: uuid <<FK>>
    * schedule_id: uuid <<FK>>
}

entity "SCHEDULE" as schedule {
    @ref schedule_service
}

entity "USER_REGISTER_JOIN_SEMESTER" as user_register_join_semester {
    * id: uuid <<PK>>
    --
    * student_id: uuid <<FK>>
    * semester_course_id: uuid <<FK>>
    * payer_id: uuid <<FK>>
    * price: float8
    * time: timestamp
}

entity "USER_REGISTER_TEACH_SEMESTER" as user_register_teach_semester {
    * id: uuid <<PK>>
    --
    * teacher_id: uuid <<FK>>
    * semester_course_id: uuid <<FK>>
    * time: timestamp
}

' Semester Relationship

user ||..o{ user_register_join_semester: pay for\n joining
user ||..o{ semester_creation: create semester
user_review_create_semester }o--|| user
user_review_create_semester ||--|| semester_creation
user_register_teach_semester }o..|| user
user_register_teach_semester }o..|| semester_course
user_register_join_semester }o..|| user
user_register_join_semester }o..|| semester_course
semester_course }o..|| course
semester_course }o..|| semester_creation
semester_course }o..|| schedule
@enduml
```

## 7. Class service

```plantuml
@startuml class_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "CLASS" as class {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * registration_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

entity "CLASS_HAS_REGISTER_JOIN_SEMESTER" as user_join_class {
    * class_id: uuid <<PK,FK>>
    * join_registration_id: uuid <<PK,FK>>
}

entity "USER_REGISTER_JOIN_SEMESTER" as user_register_join_semester {
    @ref semester_service
}

entity "USER_REGISTER_TEACH_SEMESTER" as user_register_teach_semester {
    @ref semester_service
}

' Class Relationship

user ||..o{ class: create class
user_join_class }o--|| class
user_join_class ||--|| user_register_join_semester
class ||..|| user_register_teach_semester: register in
@enduml
```

## 8. Section service

```plantuml
@startuml section_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "SECTION" as section {
    * id: uuid <<PK>>
    --
    make_up_for_section_id: uuid <<FK>>
    * class_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * number: integer
    * create_time: timestamp
    * update_time: timestamp
}

entity "SECTION_TEMPLATE" as section_template {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * course_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * number: integer
    * create_time: timestamp
    * update_time: timestamp
}

entity "CLASS" as class {
    @ref class_service
}

entity "COURSE" as course {
    @ref course_service
}

' Section Relationship
section ||..|| section: make up for
class ||..o{ section
section_template }o..|| course
section_template }o..|| user
@enduml
```

## 9. Exercise service

```plantuml
@startuml exercise_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "SECTION" as section {
    @ref section_service
}

entity "SECTION_TEMPLATE" as section_template {
    @ref section_service
}

entity "EXERCISE_LEVEL" as exercise_level {
    * id: uuid <<PK>>
    --
    * name: varchar(255)
    * description: text
    * weight: float8
}

entity "EXERCISE" as exercise {
    * id: uuid <<PK>>
    --
    * section_id: uuid <<FK>>
    * level_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

entity "EXERCISE_TEMPLATE" as exercise_template {
    * id: uuid <<PK>>
    --
    * section_template_id: uuid <<FK>>
    * level_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

' Exercise Relationship
exercise }o..|| exercise_level
exercise }o..|| section
exercise_template }o..|| exercise_level
exercise_template }o..|| section_template
@enduml
```

## 10. Tutorial service

```plantuml
@startuml tutorial_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "SECTION" as section {
    @ref section_service
}

entity "SECTION_TEMPLATE" as section_template {
    @ref section_service
}

entity "TUTORIAL" as tutorial {
    * id: uuid <<PK>>
    --
    * section_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

entity "TUTORIAL_TEMPLATE" as tutorial_template {
    * id: uuid <<PK>>
    --
    * section_template_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * create_time: timestamp
    * update_time: timestamp
}

entity "TUTORIAL_PAGE" as tutorial_page {
    * id: uuid <<PK>>
    --
    * tutorial_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    * number: integer
}

' Tutorial Relationship

tutorial ||..o{ tutorial_page
tutorial }o..|| section
tutorial_template ||..o{ tutorial_page
tutorial_template }o..|| section_template
@enduml
```

## 11. Submission service

```plantuml
@startuml submission_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "EXERCISE_SUBMISSION" as exercise_submission {
    * id: uuid <<PK>>
    --
    * student_id: uuid <<FK>>
    * exercise_id: uuid <<FK>>
    * image_url: varchar(255)
    * create_time: timestamp
    * update_time: timestamp
}

entity "CONTEST_SUBMISSION" as contest_submission {
    * id: uuid <<PK>>
    --
    * student_id: uuid <<FK>>
    * contest_id: uuid <<FK>>
    * image_url: varchar(255)
    * create_time: timestamp
    * update_time: timestamp
}

entity "USER_GRADE_EXERCISE_SUBMISSION" as user_grade_exercise_submission {
    * teacher_id: uuid <<PK,FK>>
    * exercise_submission_id: uuid <<PK,FK>>
    --
    * feedback: text
    * score: float8
    * time: timestamp
}

entity "USER_GRADE_CONTEST_SUBMISSION" as user_grade_contest_submission {
    * teacher_id: uuid <<PK,FK>>
    * contest_submission_id: uuid <<PK,FK>>
    --
    * feedback: text
    * score: float8
    * time: timestamp
}

entity "EXERCISE" as exercise {
    @ref exercise_service
}

entity "CONTEST" as contest {
    @ref contest_service
}

' Submission Relationship

exercise_submission }o..|| user
exercise_submission }o..|| exercise
contest_submission }o..|| user
contest_submission }o..|| contest
user_grade_exercise_submission ||--|| user
user_grade_exercise_submission }o--|| exercise_submission
user_grade_contest_submission }o--|| user
user_grade_contest_submission }o--|| contest_submission
@enduml
```

## 12. Notification service

```plantuml
@startuml notification_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "CLASS" as class {
    @ref class_service
}

entity "NOTIFICATION" as notification {
    * id: uuid <<PK>>
    --
    * name: varchar(255)
    * description: text
    * time: timestamp
}

entity "ANONYMOUS_NOTIFICATION" as anonymous_notification {
    * notification_id: uuid <<PK>>
    --
    * email: varchar(255)
    * user_full_name: varchar(255)
    * phone: varchar(15)
}

entity "CLASS_NOTIFICATION" as class_notification {
    * notification_id: uuid <<PK>>
    --
    * class_id: uuid <<FK>>
}

entity "USER_READ_NOTIFICATION" as user_read_notification {
    * user_id: uuid <<PK,FK>>
    * notification_id: uuid <<PK,FK>>
    --
    * is_read: boolean
}

' Notification Relationship

notification ||--|| anonymous_notification
notification ||--|| class_notification
notification ||--o{ user_read_notification
class_notification ||..|| class
user_read_notification }o--|| user
@enduml
```

## 13. Blog service

```plantuml
@startuml blog_service
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "USER" as user {
    @ref user_service
}

entity "BLOG" as blog {
    * id: uuid <<PK>>
    --
    * creator_id: uuid <<FK>>
    * name: varchar(255)
    * description: text
    image_url: varchar(255)
    * create_time: timestamp
    * update_time: timestamp
}

entity "TAG" as tag {
    * id: uuid <<PK>>
    --
    * name: varchar(255)
}

entity "BLOG_HAS_TAG" as blog_has_tag {
    * blog_id: uuid <<PK,FK>>
    * tag_id: uuid <<PK,FK>>
}

' Notification Relationship

user ||..o{ blog: create blog
blog_has_tag }o--|| blog
blog_has_tag }o--|| tag
@enduml
```
