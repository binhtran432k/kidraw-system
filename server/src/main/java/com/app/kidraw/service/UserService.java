package com.app.kidraw.service;

import com.app.kidraw.dto.PageDto;
import com.app.kidraw.dto.PageGetDto;
import com.app.kidraw.dto.UserAllDto;
import com.app.kidraw.dto.UserDetailDto;
import com.app.kidraw.dto.UserServiceDto;

public interface UserService {
    // Create
    // UserDto create(UserServiceDto userServiceDto);
    //
    // // Read
    // PageDto<UserAllDto> getAll(PageGetDto pageGetDto);
    //
    // PageDto<UserAllDto> getAllWithRoleName(String roleName, PageGetDto pageGetDto);
    //
    // UserDetailDto getById(String id);
    //
    // UserDetailDto getByEmail(String email);
    //
    // UserDetailDto getByUsername(String username);
    //
    // PageDto<UserAllDto> searchAll(String searchStr, PageGetDto pageGetDto);
    //
    // PageDto<UserAllDto> searchAllWithRoleName(String searchStr, String roleName, PageGetDto pageGetDto);
    //
    // // Update
    // boolean update(UserServiceDto userServiceDto);
    //
    // // Delete
    // boolean deleteById(String id);
}
