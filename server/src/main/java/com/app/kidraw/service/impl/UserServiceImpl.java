package com.app.kidraw.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.app.kidraw.entity.Role;
import com.app.kidraw.entity.User;
import com.app.kidraw.exception.EntityNotFoundException;
import com.app.kidraw.exception.UserAlreadyRegisteredException;
import com.app.kidraw.repository.RoleRepository;
import com.app.kidraw.repository.UserRepository;
import com.app.kidraw.service.UserService;
import com.app.kidraw.util.AuthUtil;
import com.app.kidraw.util.PageUtil;

import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Transactional
@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthUtil authUtil;
    private final PageUtil pageUtil;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("exception.user.not_found"));
    }
}
