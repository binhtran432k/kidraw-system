package com.app.kidraw.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-26
 *
 *        All bean configuration for System
 *
 */
@Configuration
public class BeanConfig {
    /**
     *
     * Message source bean for System
     * use for localization
     *
     * @return Message source bean
     */
    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setDefaultEncoding("UTF-8");
        source.setBasenames("messages/message");
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

    /**
     *
     * Password encoder bean for System
     * use for encoding password when save
     * or compare in database
     *
     * @return BCryt password encoder bean
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
