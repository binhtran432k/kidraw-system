package com.app.kidraw;

import com.app.kidraw.service.UserService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-26
 *
 *        Main Controller of system
 *
 */
@SpringBootApplication
public class KidspaintingApplication {
    public static void main(String[] args) {
        SpringApplication.run(KidspaintingApplication.class, args);
    }

    CommandLineRunner run(UserService userService) {
        return args -> {
        };
    }
}
