package com.app.kidraw.util.constant;

public class UserRoleConstant {
    private UserRoleConstant() {}

    public static final String ADMIN = "ROLE_ADMIN";
    public static final String STUDENT = "ROLE_STUDENT";
    public static final String CHILDREN = "ROLE_CHILDREN";
    public static final String STAFF = "ROLE_STAFF";
    public static final String TEACHER = "ROLE_TEACHER";
}
