package com.app.kidraw.util.constant;

public class ResponseMessageConstant {
    private ResponseMessageConstant() {}

    public static final String USER_NOT_FOUND = "exception.user.not_found";
    public static final String USERNAME_TAKEN = "exception.user.username_taken";
    public static final String EMAIL_TAKEN = "exception.user.email_taken";
    public static final String ROLE_INVALID = "exception.role.invalid";
    public static final String JWT_INVALID = "exception.jwt.invalid";
    public static final String JWT_BEARER_INVALID = "exception.jwt.bearer_invalid";
}
