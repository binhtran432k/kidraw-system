package com.app.kidraw.entity;


import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 * Contest entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "contest")
public class Contest extends BaseStudy {
    @Column(name = "registration_time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime registrationTime;

    @Column(name = "start_time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime endTime;

    @Column(name = "max_participant", nullable = false, columnDefinition = "integer default 0")
    private Integer maxParticipant;

    @Column(name = "visibility", nullable = false, columnDefinition = "boolean default false")
    private Boolean visibility;

    @OneToMany(mappedBy = "contest",
            targetEntity = ContestSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<ContestSubmission> submissions;

    @ManyToMany(mappedBy = "usersJoinedContests",
            targetEntity = User.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<User> joinedUsers;

    @ManyToMany(mappedBy = "usersGradeContests",
            targetEntity = User.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<User> gradeUsers;
}
