package com.app.kidraw.entity;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.app.kidraw.entity.enumerate.EmojiType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 *        Like entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass
public class BaseEmoji extends BaseEntity {
    @Column(name = "count", nullable = false, columnDefinition = "integer default 1")
    private Long count;

    @Column(name = "type", nullable = false, columnDefinition = "char(4) default 'LIKE'")
    private EmojiType type;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
}
