package com.app.kidraw.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass
public class BaseContent extends BaseEntity {
    @Column(name = "name",
            nullable = false,
            unique = true)
    private String name;

    @Column(name = "description",
            nullable = false,
            columnDefinition = "text")
    private String description;
}

