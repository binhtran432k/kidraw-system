package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.app.kidraw.entity.enumerate.RelationshipStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "relationship")
public class Relationship extends BaseEntity {
    @Column(name = "status", columnDefinition = "char(4)")
    private RelationshipStatus status;

    @Column(name = "time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "user_one_id", referencedColumnName = "id")
    private User userOne;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "user_two_id", referencedColumnName = "id")
    private User userTwo;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "action_user_id", referencedColumnName = "id")
    private User actionUser;

    @OneToMany(mappedBy = "relationship", targetEntity = Message.class, cascade = CascadeType.ALL)
    private Set<Message> messageList;
}
