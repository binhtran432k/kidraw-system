package com.app.kidraw.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.app.kidraw.entity.enumerate.Sex;

import org.hibernate.annotations.Where;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "user")
@Where(clause = "is_deleted = false")
@Transactional
public class User extends BaseEntity implements UserDetails {
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "sex", columnDefinition = "char(1)")
    private Sex sex;

    @Column(name = "phone", columnDefinition = "char(10)")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "create_time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime createTime;

    @Column(name = "profile_image_url")
    private String profileImageUrl;

    @Column(name = "background_image_url")
    private String backgroundImageUrl;

    @Column(name = "is_online", nullable = false, columnDefinition = "boolean default false")
    private Boolean isOnline;

    @Column(name = "is_deleted", nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted;

    @Column(name = "is_account_non_expired", nullable = false, columnDefinition = "boolean default true")
    private Boolean isAccountNonExpired;

    @Column(name = "is_account_non_locked", nullable = false, columnDefinition = "boolean default true")
    private Boolean isAccountNonLocked;

    @Column(name = "is_credit_card_non_expired", nullable = false, columnDefinition = "boolean default true")
    private Boolean isCreditCardNonExpired;
    
    @Column(name = "is_enabled", nullable = false, columnDefinition = "boolean default true")
    private Boolean isEnabled;

    @OneToMany(mappedBy = "creator",
            targetEntity = Course.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Course> createdCourses;

    @OneToMany(mappedBy = "creator",
            targetEntity = ClassEntity.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<ClassEntity> createdClasses;

    @OneToMany(mappedBy = "teacher",
            targetEntity = ClassEntity.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<ClassEntity> teachClasses;

    @OneToMany(mappedBy = "teacher",
            targetEntity = UserGradeSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<UserGradeSubmission> gradeSubmissions;

    @OneToMany(mappedBy = "submitter",
            targetEntity = BaseSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<ExerciseSubmission> submitExerciseSubmissions;

    @OneToMany(mappedBy = "submitter",
            targetEntity = BaseSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<ContestSubmission> submitContestSubmissions;

    @OneToMany(mappedBy = "creator",
            targetEntity = Blog.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Blog> createdBlog;

    @OneToMany(mappedBy = "userOne",
            targetEntity = Relationship.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Relationship> userOneRelationships;

    @OneToMany(mappedBy = "userTwo",
            targetEntity = Relationship.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Relationship> userTwoRelationships;

    @OneToMany(mappedBy = "actionUser",
            targetEntity = Relationship.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Relationship> actionUserRelationships;

    @OneToMany(mappedBy = "user",
            targetEntity = Picture.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Picture> pictures;

    @OneToMany(mappedBy = "user",
            targetEntity = PostEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<PostEmoji> postEmojis;

    @OneToMany(mappedBy = "user",
            targetEntity = MessageEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<MessageEmoji> messageEmojis;

    @OneToMany(mappedBy = "user",
            targetEntity = CommentEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<CommentEmoji> commentEmojis;

    @OneToMany(mappedBy = "loggedInUser",
            targetEntity = Post.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Post> userPosts;

    @OneToMany(mappedBy = "timelineUser",
            targetEntity = Post.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Post> timelineUserPosts;

    @OneToMany(mappedBy = "creator",
            targetEntity = Comment.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Comment> createdComments;

    @OneToMany(mappedBy = "timelineUser",
            targetEntity = Comment.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Comment> timelineUserComments;

    @OneToMany(mappedBy = "toUser",
            targetEntity = Message.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Message> receivedMessages; // user is in "to user"

    @OneToMany(mappedBy = "fromUser",
            targetEntity = Message.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Message> sendedMessages; // user is in "from user"

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "users_joined_classes",
            joinColumns = { @JoinColumn(name = "user_id",
                    referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "class_id",
                    referencedColumnName = "id") })
    private Set<ClassEntity> usersJoinedClasses;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "users_joined_contests",
            joinColumns = { @JoinColumn(name = "user_id",
                    referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "contest_id",
                    referencedColumnName = "id") })
    private Set<Contest> usersJoinedContests;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "users_grade_contests",
            joinColumns = { @JoinColumn(name = "user_id",
                    referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "contest_id",
                    referencedColumnName = "id") })
    private Set<Contest> usersGradeContests;

    @Builder.Default
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "users_have_roles",
            joinColumns = { @JoinColumn(name = "user_id",
                    referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id",
                    referencedColumnName = "id") })
    private Set<Role> authorities = new HashSet<>(); // Initial for security configuration

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
