package com.app.kidraw.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
// import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_grade_submission")
public class UserGradeSubmission {
    @Getter
    @Setter
    @AllArgsConstructor
    class UserGradeSubmissionId implements Serializable {
        @Column(name = "teacher_id")
        private String teacherId;

        @Column(name = "submission_id")
        private String submissionId;

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
            return true;

            if (obj == null || getClass() != obj.getClass())
            return false;

            UserGradeSubmissionId that = (UserGradeSubmissionId)obj;
            return Objects.equals(teacherId, that.teacherId) &&
            Objects.equals(submissionId, that.submissionId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(teacherId, submissionId);
        }
    }

    @EmbeddedId
    private UserGradeSubmissionId id;

    @Column(name = "score",
            nullable = false,
            columnDefinition = "float8 default 0")
    private Double score;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @MapsId("teacherId")
    private User teacher;

    @ManyToOne(targetEntity = BaseSubmission.class,
            fetch = FetchType.LAZY)
    @MapsId("submissionId")
    private BaseSubmission submission;

    // @ManyToOne(targetEntity = ContestSubmission.class,
    //         fetch = FetchType.LAZY)
    // // @JoinColumn(name = "submission_id", referencedColumnName = "id")
    // @MapsId("submissionId")
    // private ContestSubmission contestSubmission;
    //
    // @ManyToOne(targetEntity = ExerciseSubmission.class,
    //         fetch = FetchType.LAZY)
    // @JoinColumn(name = "submission_id", referencedColumnName = "id")
    // private ExerciseSubmission exerciseSubmission;
}
