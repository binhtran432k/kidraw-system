package com.app.kidraw.entity.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageStatus {
    // TODO: list all message status
    LIKE("LIKE"),
    UNLIKE("UNLI"),
    LOVE("LOVE"),
    SAD("SAD"),
    SUPRISE("SUPR"),
    ANGRY("ANGR");

    private String code;
}
