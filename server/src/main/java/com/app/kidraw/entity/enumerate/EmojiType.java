package com.app.kidraw.entity.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmojiType {
    LIKE("LIKE"),
    UNLIKE("UNLI"),
    LOVE("LOVE"),
    SAD("SAD"),
    SUPRISE("SUPR"),
    ANGRY("ANGR");

    private String code;
}
