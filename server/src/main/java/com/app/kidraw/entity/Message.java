package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.app.kidraw.entity.enumerate.MessageStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "message")
public class Message extends BaseEntity {
    @Column(name = "subject")
    private String subject;

    @Column(name = "content", nullable = false, columnDefinition = "text")
    private String content;

    @Column(name = "status", columnDefinition = "char(4)")
    private MessageStatus status;

    @Column(name = "time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "from_user_id", referencedColumnName = "id")
    private User fromUser;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "to_user_id", referencedColumnName = "id")
    private User toUser;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "relationship_id", referencedColumnName = "id")
    private Relationship relationship;

    @OneToMany(mappedBy = "message",
            targetEntity = MessageEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<MessageEmoji> emoji;
}
