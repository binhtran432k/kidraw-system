package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "section")
public class Section extends BaseEntity {
    @Column(name = "subject", nullable = false)
    private String subject;

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "start_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime startTime;

    @Column(name = "end_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime endTime;

    @Column(name = "create_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime createTime;

    @Column(name = "update_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime updateTime;

    @ManyToOne(targetEntity = ClassEntity.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id", referencedColumnName = "id")
    private ClassEntity classEntity;

    @OneToMany(mappedBy = "section",
            targetEntity = Exercise.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Exercise> exercises;
}
