package com.app.kidraw.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 *        Logger entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "logger")
public class Logger extends BaseEntity {
    @Column(name = "username")
    private String username;

    @Column(name = "method")
    private String method;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "action")
    private String action;

    @Column(name = "time", columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;
}
