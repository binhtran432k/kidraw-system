package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "exercise")
public class Exercise extends BaseEntity {
    @Column(name = "subject", nullable = false)
    private String subject; 

    @Column(name = "content", columnDefinition = "text")
    private String content;

    @Column(name = "max_submission", columnDefinition = "integer default 1")
    private Integer maxSubmission;

    @Column(name = "create_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime createTime;

    @Column(name = "update_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime updateTime;

    @ManyToOne(targetEntity = Section.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "section_id", referencedColumnName = "id")
    private Section section;

    @ManyToOne(targetEntity = ExerciseLevel.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "level_id", referencedColumnName = "id")
    private ExerciseLevel level;

    @OneToMany(mappedBy = "exercise",
            targetEntity = ExerciseSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<ExerciseSubmission> submissions;
}
