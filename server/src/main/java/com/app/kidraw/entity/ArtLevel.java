package com.app.kidraw.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 *        Art Level entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "art_level")
public class ArtLevel extends BaseContent {
    @OneToMany(mappedBy = "level",
            targetEntity = Contest.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Contest> contests;

    @OneToMany(mappedBy = "level",
            targetEntity = Course.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Course> courses;
}
