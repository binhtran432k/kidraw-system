package com.app.kidraw.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "picture")
public class Picture extends BaseEntity {
    @Column(name = "content", nullable = false, columnDefinition = "text")
    private String description;

    @Column(name = "image_url", nullable = true)
    private String imageUrl;

    @Column(name = "time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @Column(name = "cloudinary_public_id")
    private String cloudinaryPublicId;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
}
