package com.app.kidraw.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "course")
public class Course extends BaseStudy {
    @Column(name = "price",
            nullable = false,
            columnDefinition = "float8 default 0")
    private Double price;

    @Column(name = "max_participant",
            nullable = false,
            columnDefinition = "integer default 0")
    private Integer maxParticipant;

    // TODO: separate section
    @Column(name = "section_template",
            columnDefinition = "text")
    private String sectionTemplate;

    @Column(name = "visibility",
            nullable = false,
            columnDefinition = "boolean default false")
    private Boolean visibility;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id",
            referencedColumnName = "id")
    private User creator;

    @OneToMany(mappedBy = "course",
            targetEntity = ClassEntity.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<Section> sections;
}
