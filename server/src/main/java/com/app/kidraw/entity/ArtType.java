package com.app.kidraw.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 * Art Type entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "art_type")
public class ArtType extends BaseContent {
    @OneToMany(mappedBy = "type",
            targetEntity = Contest.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Contest> contests;

    @OneToMany(mappedBy = "type",
            targetEntity = Course.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Course> courses;
}
