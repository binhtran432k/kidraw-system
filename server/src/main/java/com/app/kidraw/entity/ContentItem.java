package com.app.kidraw.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "content_item")
public class ContentItem extends BaseEntity {
    @Column(name = "subject",
            nullable = false)
    private String subject;

    @Column(name = "content",
            nullable = false,
            columnDefinition = "text")
    private String content;

    @ManyToOne(targetEntity = BaseSubmission.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id",
            referencedColumnName = "id")
    private BaseSubmission submission;
}
