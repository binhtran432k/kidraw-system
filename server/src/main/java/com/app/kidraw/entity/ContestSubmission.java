package com.app.kidraw.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
// @Table(name = "contest_submission")
@DiscriminatorValue("CONTEST")
public class ContestSubmission extends BaseSubmission {
    @ManyToOne(targetEntity = Contest.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "contest_id",
            referencedColumnName = "id")
    private Contest contest;
}
