package com.app.kidraw.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 * Class entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "class")
public class ClassEntity extends BaseContent {
    @Column(name = "schedule", columnDefinition = "text")
    private String schedule;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private User teacher;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    private User creator;

    @ManyToOne(targetEntity = Course.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private Course course;

    @OneToMany(mappedBy = "classEntity",
            targetEntity = Section.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Section> sections;

    @OneToMany(mappedBy = "classEntity",
            targetEntity = Notification.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Notification> notifications;

    @ManyToMany(mappedBy = "usersJoinedClasses",
            targetEntity = User.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH)
    private Set<User> joinedUsers;
}
