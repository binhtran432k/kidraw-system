package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "base_submission")
@DiscriminatorColumn(name = "submission_type")
public class BaseSubmission extends BaseEntity {
    @Column(name = "feedback",
            columnDefinition = "text")
    private String feedback;

    @Column(name = "time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "submitter_id",
            referencedColumnName = "id")
    private User submitter;

    @OneToMany(mappedBy = "submission",
            targetEntity = FileItem.class,
            cascade = CascadeType.ALL)
    private Set<FileItem> fileSubmitItems;

    @OneToMany(mappedBy = "submission",
            targetEntity = ContentItem.class,
            cascade = CascadeType.ALL)
    private Set<ContentItem> contentSubmitItems;

    @OneToMany(mappedBy = "submission",
            targetEntity = UserGradeSubmission.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<UserGradeSubmission> userGradeSubmissions;
}
