package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 * Comment entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "comment")
public class Comment extends BaseEntity {
    @Column(name = "content", nullable = false, columnDefinition = "text")
    private String content;

    @Column(name = "time", nullable = false, columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @ManyToOne(targetEntity = Post.class,
            optional = false,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", referencedColumnName = "id")
    private Post post;

    @ManyToOne(targetEntity = User.class,
            optional = false,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "creater_id", referencedColumnName = "id")
    private User creator;

    @ManyToOne(targetEntity = User.class,
            optional = false,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "timeline_user_id", referencedColumnName = "id")
    private User timelineUser;

    @OneToMany(mappedBy = "comment",
            targetEntity = CommentEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<CommentEmoji> emoji;
}
