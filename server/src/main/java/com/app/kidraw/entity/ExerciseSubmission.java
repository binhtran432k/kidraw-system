package com.app.kidraw.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
// @Table(name = "exercise_submission")
@DiscriminatorValue("EXERCISE")
public class ExerciseSubmission extends BaseSubmission {
    @ManyToOne(targetEntity = Exercise.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "exercise_id",
            referencedColumnName = "id")
    private Exercise exercise;
}
