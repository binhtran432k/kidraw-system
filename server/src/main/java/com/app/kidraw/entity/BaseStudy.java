package com.app.kidraw.entity;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 * Study base for course and contest
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass
public class BaseStudy extends BaseContent {
    @ManyToOne(targetEntity = ArtType.class,
            optional = false,
            fetch = FetchType.EAGER,
            cascade = CascadeType.DETACH)
    @JoinColumn(name = "art_type_id", referencedColumnName = "id")
    private ArtType type;

    @ManyToOne(targetEntity = ArtLevel.class,
            optional = false,
            fetch = FetchType.EAGER,
            cascade = CascadeType.DETACH)
    @JoinColumn(name = "art_level_id", referencedColumnName = "id")
    private ArtLevel level;
}
