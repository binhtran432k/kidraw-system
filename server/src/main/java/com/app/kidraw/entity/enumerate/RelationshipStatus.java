package com.app.kidraw.entity.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RelationshipStatus {
    // TODO: list all relationship status
    LIKE("LIKE"),
    UNLIKE("UNLI"),
    LOVE("LOVE"),
    SAD("SAD"),
    SUPRISE("SUPR"),
    ANGRY("ANGR");

    private String code;
}
