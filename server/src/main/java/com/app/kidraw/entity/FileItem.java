package com.app.kidraw.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "file_item")
public class FileItem extends BaseEntity {
    @Column(name = "fileUrl",
            nullable = false)
    private String fileUrl;

    @ManyToOne(targetEntity = BaseSubmission.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id",
            referencedColumnName = "id")
    private BaseSubmission submission;
}
