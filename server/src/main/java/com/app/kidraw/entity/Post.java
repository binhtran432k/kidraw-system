package com.app.kidraw.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Tran Duc Binh
 * @version 1.0
 * @since 2022-03-28
 *
 *        Post entity in database
 *
 */
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "post")
public class Post extends BaseEntity {
    @Column(name = "content", nullable = false, columnDefinition = "text")
    private String content;

    @Column(name = "image_url", nullable = true)
    private String imageUrl;

    @Column(name = "time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime time;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "logged_in_user_id", referencedColumnName = "id")
    private User loggedInUser;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "timeline_user_id", referencedColumnName = "id")
    private User timelineUser;

    @OneToMany(mappedBy = "post",
            targetEntity = PostEmoji.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<PostEmoji> emojis;

    @OneToMany(mappedBy = "post",
            targetEntity = Comment.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Comment> comments;
}
