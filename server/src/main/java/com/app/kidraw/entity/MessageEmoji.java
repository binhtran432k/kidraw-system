package com.app.kidraw.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "message_emoji")
public class MessageEmoji extends BaseEmoji {
    @ManyToOne(targetEntity = Message.class)
    @JoinColumn(name = "message_id", referencedColumnName = "id")
    private Message message;
}
