package com.app.kidraw.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "blog")
public class Blog extends BaseContent {
    @Column(name = "tags", columnDefinition = "text")
    private String tags;

    @Column(name = "create_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime createTime;

    @Column(name = "update_time",
            nullable = false,
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime updateTime;

    @ManyToOne(targetEntity = User.class,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    private User creator;
}
