package com.app.kidraw.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "exercise_level")
public class ExerciseLevel extends BaseEntity {
    @Column(name = "subject", nullable = false)
    private String subject;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "weight", columnDefinition = "float8 default 0")
    private Double weight;

    @OneToMany(mappedBy = "level",
            targetEntity = Exercise.class,
            cascade = CascadeType.ALL)
    private List<Exercise> exercises;
}
