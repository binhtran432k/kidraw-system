package com.app.kidraw.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageGetDto {
    private int size;
    private int page;
    private String[] sort;
}

