package com.app.kidraw.dto;

import java.util.List;

import lombok.Data;

@Data
public class PageDto<T> {
    private List<T> items;
    private int index;
    private long totalItems;
    private int totalPages;
}

