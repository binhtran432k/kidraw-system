package com.app.kidraw.exception;

import com.app.kidraw.exception.config.GlobalErrorCode;

public class JWTValidationException extends CustomGlobalException {
    public JWTValidationException(String message) {
        super(message, GlobalErrorCode.ERROR_JWT_VALIDATION_REGISTERED);
    }
}
