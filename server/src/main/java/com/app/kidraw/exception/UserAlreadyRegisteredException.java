package com.app.kidraw.exception;

import com.app.kidraw.exception.config.GlobalErrorCode;

public class UserAlreadyRegisteredException extends CustomGlobalException {
    public UserAlreadyRegisteredException(String message) {
        super(message, GlobalErrorCode.ERROR_USER_ALREADY_REGISTERED);
    }
}
