
    create table art_level (
       id varchar(255) not null,
        description text,
        subject varchar(255) not null,
        primary key (id)
    );

    create table art_type (
       id varchar(255) not null,
        description text,
        subject varchar(255) not null,
        primary key (id)
    );

    create table base_submission (
       submission_type varchar(31) not null,
        id varchar(255) not null,
        feedback text,
        time timestamp default current_timestamp not null,
        submitter_id varchar(255),
        contest_id varchar(255),
        exercise_id varchar(255),
        primary key (id)
    );

    create table blog (
       id varchar(255) not null,
        content text not null,
        create_time timestamp default current_timestamp not null,
        subject varchar(255) not null,
        tags text,
        update_time timestamp default current_timestamp not null,
        creator_id varchar(255),
        primary key (id)
    );

    create table class (
       id varchar(255) not null,
        description text,
        name varchar(255) not null,
        schedule text,
        course_id varchar(255),
        creator_id varchar(255),
        teacher_id varchar(255),
        primary key (id)
    );

    create table comment (
       id varchar(255) not null,
        content text not null,
        image_url varchar(255) not null,
        time timestamp default current_timestamp not null,
        creater_id varchar(255) not null,
        post_id varchar(255) not null,
        timeline_user_id varchar(255) not null,
        primary key (id)
    );

    create table comment_emoji (
       id varchar(255) not null,
        count bigint default 0 not null,
        type char(4) default 'LIKE' not null,
        user_id varchar(255),
        comment_id varchar(255),
        primary key (id)
    );

    create table content_item (
       id varchar(255) not null,
        content text not null,
        subject varchar(255) not null,
        submission_id varchar(255),
        primary key (id)
    );

    create table contest (
       id varchar(255) not null,
        content text not null,
        subject varchar(255) not null,
        end_time timestamp default current_timestamp not null,
        max_participant integer default 0 not null,
        registration_time timestamp default current_timestamp not null,
        start_time timestamp default current_timestamp not null,
        visibility boolean default false not null,
        art_level_id varchar(255) not null,
        art_type_id varchar(255) not null,
        primary key (id)
    );

    create table course (
       id varchar(255) not null,
        content text not null,
        subject varchar(255) not null,
        max_participant integer default 0 not null,
        price float8 default 0 not null,
        section_template text,
        visibility boolean default false not null,
        art_level_id varchar(255) not null,
        art_type_id varchar(255) not null,
        creator_id varchar(255),
        primary key (id)
    );

    create table exercise (
       id varchar(255) not null,
        content text,
        create_time timestamp default current_timestamp not null,
        max_submission integer default 1,
        subject varchar(255) not null,
        update_time timestamp default current_timestamp not null,
        level_id varchar(255),
        section_id varchar(255),
        primary key (id)
    );

    create table exercise_level (
       id varchar(255) not null,
        description text,
        subject varchar(255) not null,
        weight float8 default 0,
        primary key (id)
    );

    create table file_item (
       id varchar(255) not null,
        fileUrl varchar(255) not null,
        submission_id varchar(255),
        primary key (id)
    );

    create table logger (
       id varchar(255) not null,
        action varchar(255),
        method varchar(255),
        table_name varchar(255),
        time timestamp default current_timestamp,
        username varchar(255),
        primary key (id)
    );

    create table message (
       id varchar(255) not null,
        content text not null,
        status char(4),
        subject varchar(255),
        time timestamp default current_timestamp not null,
        from_user_id varchar(255) not null,
        relationship_id varchar(255) not null,
        to_user_id varchar(255) not null,
        primary key (id)
    );

    create table message_emoji (
       id varchar(255) not null,
        count bigint default 0 not null,
        type char(4) default 'LIKE' not null,
        user_id varchar(255),
        message_id varchar(255),
        primary key (id)
    );

    create table notification (
       id varchar(255) not null,
        content text not null,
        subject varchar(255) not null,
        time timestamp default current_timestamp not null,
        class_id varchar(255),
        primary key (id)
    );

    create table picture (
       id varchar(255) not null,
        cloudinary_public_id varchar(255),
        content text not null,
        image_url varchar(255),
        time timestamp default current_timestamp not null,
        user_id varchar(255),
        primary key (id)
    );

    create table post (
       id varchar(255) not null,
        content text not null,
        image_url varchar(255),
        time timestamp default current_timestamp not null,
        logged_in_user_id varchar(255),
        timeline_user_id varchar(255),
        primary key (id)
    );

    create table post_emoji (
       id varchar(255) not null,
        count bigint default 0 not null,
        type char(4) default 'LIKE' not null,
        user_id varchar(255),
        post_id varchar(255),
        primary key (id)
    );

    create table relationship (
       id varchar(255) not null,
        status char(4),
        time timestamp default current_timestamp not null,
        action_user_id varchar(255) not null,
        user_one_id varchar(255) not null,
        user_two_id varchar(255) not null,
        primary key (id)
    );

    create table role (
       id varchar(255) not null,
        authority varchar(255) not null,
        description varchar(255),
        primary key (id)
    );

    create table section (
       id varchar(255) not null,
        content text,
        create_time timestamp default current_timestamp not null,
        end_time timestamp default current_timestamp not null,
        start_time timestamp default current_timestamp not null,
        subject varchar(255) not null,
        update_time timestamp default current_timestamp not null,
        class_id varchar(255),
        primary key (id)
    );

    create table user (
       id varchar(255) not null,
        address varchar(255),
        background_image_url varchar(255),
        create_time timestamp default current_timestamp not null,
        date_of_birth date,
        email varchar(255),
        first_name varchar(255),
        is_account_non_expired boolean default true not null,
        is_account_non_locked boolean default true not null,
        is_credit_card_non_expired boolean default true not null,
        is_deleted boolean default false not null,
        is_enabled boolean default true not null,
        is_online boolean default false not null,
        last_name varchar(255),
        password varchar(255) not null,
        phone varchar(255),
        profile_image_url varchar(255),
        sex varchar(255),
        username varchar(255) not null,
        primary key (id)
    );

    create table user_grade_submission (
       score float8 default 0 not null,
        submission_id varchar(255) not null,
        teacher_id varchar(255) not null,
        primary key (submission_id, teacher_id)
    );

    create table users_grade_contests (
       user_id varchar(255) not null,
        contest_id varchar(255) not null,
        primary key (user_id, contest_id)
    );

    create table users_have_roles (
       user_id varchar(255) not null,
        role_id varchar(255) not null,
        primary key (user_id, role_id)
    );

    create table users_joined_classes (
       user_id varchar(255) not null,
        class_id varchar(255) not null,
        primary key (user_id, class_id)
    );

    create table users_joined_contests (
       user_id varchar(255) not null,
        contest_id varchar(255) not null,
        primary key (user_id, contest_id)
    );

    alter table art_level 
       add constraint UK_dcqva1pb090rmjlm8lre52qu0 unique (subject);

    alter table art_type 
       add constraint UK_b381u13mrvvwgwsksiofje9l7 unique (subject);

    alter table blog 
       add constraint UK_p7fq0rbxk8ylc7ypgj0j4d3qw unique (subject);

    alter table class 
       add constraint UK_k9k2qotp6nupi0e2ahpl0bhrp unique (name);

    alter table contest 
       add constraint UK_8ohrexkrwlm1ewf0sc929g9p0 unique (subject);

    alter table course 
       add constraint UK_pydarkl72t18rxmf0t3oi03jp unique (subject);

    alter table role 
       add constraint UK_irsamgnera6angm0prq1kemt2 unique (authority);

    alter table user 
       add constraint UK_ob8kqyqqgmefl0aco34akdtpe unique (email);

    alter table user 
       add constraint UK_sb8bbouer5wak8vyiiy4pf2bx unique (username);

    alter table base_submission 
       add constraint FKnxrf44rh015uvtsrvdkm7kuem 
       foreign key (submitter_id) 
       references user;

    alter table base_submission 
       add constraint FK7mmjb6m04rfyatkrm707ub4gr 
       foreign key (contest_id) 
       references contest;

    alter table base_submission 
       add constraint FKfyx4a1jm43190vspql6swmun6 
       foreign key (exercise_id) 
       references exercise;

    alter table blog 
       add constraint FKm2kni23xoo9vjen1mpm5gxkys 
       foreign key (creator_id) 
       references user;

    alter table class 
       add constraint FKlsxcyh4sq20727qj0clvah8dg 
       foreign key (course_id) 
       references course;

    alter table class 
       add constraint FKfvehqkaor8vakf9l4ed9p9c9r 
       foreign key (creator_id) 
       references user;

    alter table class 
       add constraint FKkjmf1ni8hw6cdospr7o96va9h 
       foreign key (teacher_id) 
       references user;

    alter table comment 
       add constraint FK8hwrr991dsgnmq1hwkcnym3f7 
       foreign key (creater_id) 
       references user;

    alter table comment 
       add constraint FKs1slvnkuemjsq2kj4h3vhx7i1 
       foreign key (post_id) 
       references post;

    alter table comment 
       add constraint FKl7f1ooi7dfd2wnpg2sc0eo9b1 
       foreign key (timeline_user_id) 
       references user;

    alter table comment_emoji 
       add constraint FKmf1x9wvicjhjvbftm813nr0y8 
       foreign key (user_id) 
       references user;

    alter table comment_emoji 
       add constraint FKb28tblfw8e9fbv9qoao5kkxh6 
       foreign key (comment_id) 
       references comment;

    alter table content_item 
       add constraint FKltmjtaqvgn7dehj3y3qfrwx0p 
       foreign key (submission_id) 
       references base_submission;

    alter table contest 
       add constraint FKbq2ub1wjpcue6e2rg4n1g9kdw 
       foreign key (art_level_id) 
       references art_level;

    alter table contest 
       add constraint FKfo2r4613o7o9cijdtel577ukn 
       foreign key (art_type_id) 
       references art_type;

    alter table course 
       add constraint FKoh4fn9sbvdouy7w0h1g5rjhji 
       foreign key (art_level_id) 
       references art_level;

    alter table course 
       add constraint FKa3x7hr86q6un13ti9usg57hhd 
       foreign key (art_type_id) 
       references art_type;

    alter table course 
       add constraint FK1isig5e63r0u4klsxr1qfk7dt 
       foreign key (creator_id) 
       references user;

    alter table exercise 
       add constraint FK21apsisgnfy1bdu2sfasg0poj 
       foreign key (level_id) 
       references exercise_level;

    alter table exercise 
       add constraint FK235ei3nje0ajthgcps3lpkrwj 
       foreign key (section_id) 
       references section;

    alter table file_item 
       add constraint FK48h3eat0bva243mfy8ns1s0s1 
       foreign key (submission_id) 
       references base_submission;

    alter table message 
       add constraint FK3nju8asf4v72h0d7g6vgtx7p2 
       foreign key (from_user_id) 
       references user;

    alter table message 
       add constraint FK8u8l5cdfjj08gbg4ey0j9nw7b 
       foreign key (relationship_id) 
       references user;

    alter table message 
       add constraint FKgm8awic1hpa2cgr7pv7j8yn0 
       foreign key (to_user_id) 
       references user;

    alter table message_emoji 
       add constraint FKeuyrb6t0m25i7nlp6i3pj5krr 
       foreign key (user_id) 
       references user;

    alter table message_emoji 
       add constraint FK1ftyd0wfpiqit6imimxars6kd 
       foreign key (message_id) 
       references message;

    alter table notification 
       add constraint FK3cblj305tlptiydlyb02lxnhs 
       foreign key (class_id) 
       references class;

    alter table picture 
       add constraint FKfa3htlps9ddix2jx1spmpedko 
       foreign key (user_id) 
       references user;

    alter table post 
       add constraint FK9dhcni3nlcxclh82gwu7eaymu 
       foreign key (logged_in_user_id) 
       references user;

    alter table post 
       add constraint FKb52tr2pi71ck5gqlvjbm27ppb 
       foreign key (timeline_user_id) 
       references user;

    alter table post_emoji 
       add constraint FKs1iay9nk62673jrtmt38ve6mg 
       foreign key (user_id) 
       references user;

    alter table post_emoji 
       add constraint FKlsa3n732xksu4asyeu7iqlxw8 
       foreign key (post_id) 
       references post;

    alter table relationship 
       add constraint FKo45dnehxll6rtkph9vv85ryu9 
       foreign key (action_user_id) 
       references user;

    alter table relationship 
       add constraint FKqrnyn7dfug7p6lsnibx398k9y 
       foreign key (user_one_id) 
       references user;

    alter table relationship 
       add constraint FKgod8uv3xxb87xgmiof1ku4fex 
       foreign key (user_two_id) 
       references user;

    alter table section 
       add constraint FK8l8i27bhro0d5mjvx6xgw1w4m 
       foreign key (class_id) 
       references class;

    alter table user_grade_submission 
       add constraint FK5nnsb3sh8hfe3bp5onk81rkq6 
       foreign key (submission_id) 
       references base_submission;

    alter table user_grade_submission 
       add constraint FKneg3ymxydro97un2e9hu9rl1o 
       foreign key (teacher_id) 
       references user;

    alter table users_grade_contests 
       add constraint FKstt9jn903ao4uda5vi62vx3rb 
       foreign key (contest_id) 
       references contest;

    alter table users_grade_contests 
       add constraint FK4c1ympadm551wdro226tis9hc 
       foreign key (user_id) 
       references user;

    alter table users_have_roles 
       add constraint FKkc6rdn1w2twrhe6k3majexsul 
       foreign key (role_id) 
       references role;

    alter table users_have_roles 
       add constraint FK8xgyyp418ydntoryuqko3eucw 
       foreign key (user_id) 
       references user;

    alter table users_joined_classes 
       add constraint FK1vlrtrjvyygr9wsubxxwm7pag 
       foreign key (class_id) 
       references class;

    alter table users_joined_classes 
       add constraint FKq60422bv3dx8f03esmrdhpm9p 
       foreign key (user_id) 
       references user;

    alter table users_joined_contests 
       add constraint FKbkgrwfc33kgt0qgeoqeaaqwe3 
       foreign key (contest_id) 
       references contest;

    alter table users_joined_contests 
       add constraint FKg2hnrwk35hrvglphaw1qg4xeo 
       foreign key (user_id) 
       references user;
