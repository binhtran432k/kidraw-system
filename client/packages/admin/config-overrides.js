import { realpathSync } from 'fs'
import { resolve } from 'path'
import { DefinePlugin } from 'webpack'

const appDirectory = realpathSync(process.cwd())
const resolveApp = (relativePath) => resolve(appDirectory, relativePath)

// our packages that will now be included in the CRA build step
const appIncludes = [
  resolveApp('src'),
  resolveApp('../components/src'),
  resolveApp('../common/src'),
]

export default function override(config, env) {
  // allow importing from outside of src folder
  config.resolve.plugins = config.resolve.plugins.filter(
    (plugin) => plugin.constructor.name !== 'ModuleScopePlugin',
  )

  config.module.rules[0].include = appIncludes
  config.module.rules[1].oneOf[2].include = appIncludes
  config.module.rules[1].oneOf[2].options.plugins.push(
    require.resolve('babel-plugin-react-native-web'),
  )

  config.plugins.push(new DefinePlugin({ __DEV__: env !== 'production' }))

  return config
}
